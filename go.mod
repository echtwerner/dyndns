module gitlab.com/echtwerner/dyndns

go 1.16

require (
	github.com/aws/aws-sdk-go v1.42.25
	github.com/sirupsen/logrus v1.8.1
)
